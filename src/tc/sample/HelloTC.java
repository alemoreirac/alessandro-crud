package tc.sample; 

import java.sql.SQLException;
  
import totalcross.sys.*;

import totalcross.ui.*; 

import totalcross.ui.event.*;

import totalcross.ui.gfx.*;

import totalcross.util.*;

 

public class HelloTC extends MainWindow {

  
    public	Grid grid;

  	private Button btnDelete, btnEdit, btnInsert; 


   public HelloTC() {

      super("CRUD - Alessandro", VERTICAL_GRADIENT);

      gradientTitleStartColor = 0; gradientTitleEndColor = 0xAAAAFF;

      // sets the default user interface style to Android

      // There are others, like

      setUIStyle(Settings.Android);

      // Use font height for adjustments, not pixels

      Settings.uiAdjustmentsBasedOnFontHeight = true;

      setBackColor(0xDDDDFF);

   }

  

   // Initialize the user interface

   public void initUI() { 
	   
	   // inicializa��o da vari�vel dbcon
	   dbHelper.Init();

	   // inser��o de bot�es de navega��o
       add(btnDelete = new Button("Excluir"),CENTER,BOTTOM-15);

       add(btnInsert = new Button("Inserir"),AFTER+25,BOTTOM-15);

       add(btnEdit = new Button("Editar"),AFTER+5,BOTTOM-15);
  		
       
  		// defini��o da grid
        Rect r = getClientRect();
  		setRect(CENTER,CENTER,Settings.screenWidth,Settings.screenHeight);
  		
	String []gridCaptions = {" Titulo "," Lancamento "," Diretor ", " Categoria "};
  		int gridWidths[] = 
  		{
  			-25,
  			-25,  
  			-25,
  			-25
  		};
  		
  
  		int gridAligns[] = { CENTER, CENTER, CENTER, CENTER};
  		grid = new Grid(gridCaptions, gridWidths, gridAligns, true);
  		add(grid, LEFT+3,TOP+3,r.width,r.height-30);
  		grid.secondStripeColor = Color.getRGB(235,235,235);

		try {
			grid.setItems(dbHelper.List());
		} catch (SQLException | InvalidDateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   } 
 

	public void onEvent(Event event)
   	{
		if(event.type == ControlEvent.PRESSED)
		{
   		if ( event.target == btnDelete)
   			{
   			// utilizando o rowID para deletar, e o nome para exibir no toast
			String rowid = grid.getSelectedItem()[4]; 
			String title = grid.getSelectedItem()[0]; 
   		
				dbHelper.Remove(rowid);
				
	         Toast.show( title+" removido com sucesso!!",2000);

				try {
					grid.setItems(dbHelper.List());
				} catch (SQLException | InvalidDateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
   			}

   		if ( event.target == btnInsert)
			{
   			//navegando para a janela de inser��o
   			EditWindow edtw = new EditWindow();
      	 
   			edtw.popup();  
			
			
		} 
   		if (event.target == btnEdit)
			{
   			String x = grid.getSelectedItem()[3];
   			
   			Filme f = new Filme(grid.getSelectedItem()[0],
   					grid.getSelectedItem()[2],
   					grid.getSelectedItem()[1],
   					grid.getSelectedItem()[3],
   					Integer.parseInt(grid.getSelectedItem()[4]));

   			// navegando, por�m com o filme como par�metro para carregar os edits com os valores.
   			EditWindow edtw = new EditWindow(f);
      	 
   			edtw.popup();  
			
			
		} 
			  
			}
		else if(event.type == ControlEvent.WINDOW_CLOSED)
			try {
				grid.setItems(dbHelper.List());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidDateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
   	

   class EditWindow extends Window
   {
	   public Edit edTitulo	,edLancamento,edCategoria, edDiretor;

	   public Button btInsert, btBack;

 
	  
   	public EditWindow()
   	{


        add(new Label("Cat�logo de Filmes"),CENTER,TOP+50);

        add(new Label("T�tulo: "), LEFT,AFTER+100);

        add(edTitulo = new Edit(),LEFT,AFTER);

        add(new Label("Lan�amento"), LEFT,AFTER+50);

        add(edLancamento = new Edit(),LEFT,AFTER);

        edLancamento.setMode(Edit.DATE);

       add(new Label("Categoria"),LEFT,AFTER+50);
 

        add(edCategoria = new Edit(),LEFT,AFTER,PARENTSIZE+70,PREFERRED);

        
        add(new Label("Diretor: "), LEFT,AFTER+100);

        add(edDiretor = new Edit(),LEFT,AFTER);
            
     

        Spacer sp = new Spacer(0,0);

        add(sp, CENTER,BOTTOM-300, PARENTSIZE+10, PREFERRED);

        add(btInsert = new Button("Inserir"), BEFORE, SAME, PARENTSIZE+40, PREFERRED,sp);

        add(btBack  = new Button("Voltar"),  AFTER,  SAME, PARENTSIZE+40, PREFERRED,sp);

        btInsert.setBackColor(Color.GREEN);

        btBack.setBackColor(Color.PINK);
 

    

        Toast.posY = CENTER;
    
        dbHelper.Create();
   
   	  
   	}
   	
   	
 	public EditWindow(Filme f)
   	{


        add(new Label("Cat�logo de Filmes"),CENTER,TOP+50);

        add(new Label("T�tulo: "), LEFT,AFTER+100);

        add(edTitulo = new Edit(),LEFT,AFTER);

        add(new Label("Lan�amento"), LEFT,AFTER+50);

        add(edLancamento = new Edit(),LEFT,AFTER);

        edLancamento.setMode(Edit.DATE);

       add(new Label("Categoria"),LEFT,AFTER+50);
 

        add(edCategoria = new Edit(),LEFT,AFTER,PARENTSIZE+70,PREFERRED);

        
        add(new Label("Diretor: "), LEFT,AFTER+100);

        add(edDiretor = new Edit(),LEFT,AFTER);
            
     

        Spacer sp = new Spacer(0,0);

        add(sp, CENTER,BOTTOM-300, PARENTSIZE+10, PREFERRED);

        add(btInsert = new Button("Atualizar"), BEFORE, SAME, PARENTSIZE+40, PREFERRED,sp);

        add(btBack  = new Button("Voltar"),  AFTER,  SAME, PARENTSIZE+40, PREFERRED,sp);

        btInsert.setBackColor(Color.GREEN);

        btBack.setBackColor(Color.PINK);
 

        edLancamento.setText(f.getLancamento());
        edDiretor.setText(f.getDiretor());
        edCategoria.setText(f.getCategoria());
        edTitulo.setText(f.getTitulo());
        
        

        Toast.posY = CENTER;
    
        dbHelper.Create();
   
   	  
   	}
   	


    public void onEvent(Event e) {

    

           if(e.type == ControlEvent.PRESSED)
           {
        	     if (e.target == btInsert && btInsert.getText() == "Inserir")
        	     {

            	     if (edTitulo.getLength() == 0 || edLancamento.getLength() == 0 || edCategoria.getLength() == 0 || edDiretor.getLength() == 0)
            	    	 
            	          Toast.show("Preencha todos os campos!",2000);
            	     else{

            	    	 Filme f = null;
					
						f = new Filme(edTitulo.getText(),edDiretor.getText(),edCategoria.getText(), edLancamento.getText());
					
                      
            	    	 try {
							dbHelper.Insert(f);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InvalidDateException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

      	         Toast.show("Data inserted successfully!",2000);
            	     }
        	    	 
        	     }        
        	     
        	     if (e.target == btInsert && btInsert.getText() == "Atualizar")
        	     {

            	     if (edTitulo.getLength() == 0 || edLancamento.getLength() == 0 || edCategoria.getLength() == 0 || edDiretor.getLength() == 0)
            	    	 
            	          Toast.show("Preencha todos os campos!",2000);
            	     else{

            	    	 Filme f = new Filme();
   					    f.setTitulo(edTitulo.getText());
   					    f.setLancamento(edLancamento.getText());
						f.setDiretor(edDiretor.getText());
						f.setCategoria(edCategoria.getText());
					    f.setrowid(Integer.parseInt(grid.getSelectedItem()[4]));
                      
            	    	 try {
							dbHelper.Update(f);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (InvalidDateException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

      	         Toast.show("Dados atualizados!!",2000);
            	     }
        	    	 
        	     }       

        	    else  if (e.target == btBack)
             
                 unpop();
           
           }
    		}	
       }
   } 
    
   
