package tc.sample;

import java.sql.SQLException;

import totalcross.sql.Connection;
import totalcross.sql.DriverManager;
import totalcross.sql.Statement;
import totalcross.sys.Convert;
import totalcross.sys.Settings;
import totalcross.ui.Toast;
import totalcross.ui.dialog.MessageBox;
import totalcross.util.Date;
import totalcross.util.InvalidDateException;

public abstract class dbHelper {
	private static Connection dbcon;
	public dbHelper() {

	}
	public static void Init(){

	       try {
			dbcon = DriverManager.getConnection("jdbc:sqlite:" + Convert.appendPath(Settings.appPath, "meuDB.db"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
public static void Create (){


    try {


       Statement st = dbcon.createStatement();

       st.execute("create table if not exists movie ( titulo varchar, data_lancamento datetime, diretor varchar, categoria varchar)");
       	
       st.close();

    }

    catch (Exception e) {

       MessageBox.showException(e, true);


    }
}
	
	
public static void Remove(String rowid)
{
	  try {
			Statement st = dbcon.createStatement();
			

	         st.execute("delete from movie where ROWID = "+rowid);
	    	 
	         List();
			
			
		} catch (SQLException e) { 
			e.printStackTrace();
		} catch (InvalidDateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

}

public static void Insert(Filme filme) throws SQLException, InvalidDateException { 
	
	         Statement st = dbcon.createStatement(); 

	         st.executeUpdate("insert into movie values('"+filme.getTitulo()+"','"+filme.getLancamento()+"','"+filme.getDiretor()+"','"+filme.getCategoria()+"')");

	         st.close(); 
	   }
public static void Update(Filme filme) throws SQLException, InvalidDateException{
	
	 Statement st = dbcon.createStatement();  
     		
     st.executeUpdate("update movie"+ 
    		 " set titulo = '"+filme.getTitulo()+
    		 "', diretor =  '" +filme.getDiretor()+
    		 "', data_lancamento = '" + filme.getLancamento()+
    		 "', categoria = '" +filme.getCategoria()+
    		  "' where ROWID ="+filme.getrowid());
     		

     st.close();
}


	  public static String[][] List() throws SQLException, InvalidDateException{
		 
		Statement st = dbcon.createStatement();
		   st.execute("select ROWID, * from movie");  
		   totalcross.sql.ResultSet rs = st.getResultSet();
		     
		   
		   String[][] values = new String[10][5];
		   
		   
		   int i = 0;
		   while (rs.next())
			   {
			   values[i][0] = rs.getString("titulo");
			   values [i][1] = rs.getString("data_lancamento");
			   values [i][2] = rs.getString("diretor");
			   values [i][3] = rs.getString("categoria");
			   values [i][4] = rs.getString("ROWID");
			   i++;
			   } 
		    


		    
		  st.close();
		  
		  
		  return values;
	  }

}
