package tc.sample;

import java.sql.Date;

public class Filme {
	private String Titulo;
	private String Categoria;
	private String Lancamento;
	private String Diretor;
	private int rowid;
	
	public void setrowid(int id) {
		this.rowid = id;
	}
	
	public int getrowid() { 
		return this.rowid;
	}
	
	
	public void setTitulo(String titulo) {
		this.Titulo = titulo;
	}
	
	public String getTitulo() { 
		return this.Titulo;
	}
	
	public void setCategoria(String categoria){
		this.Categoria = categoria;
	}
	public String getCategoria(){
		return this.Categoria;
	}
	
	public void setDiretor(String diretor){
	this.Diretor = diretor;
	}
	public String getDiretor() { 
		return this.Diretor;
	}
	
	public void setLancamento( String lancamento)	{
		this.Lancamento = lancamento;
	}
	
	public String getLancamento(){
	return this.Lancamento;
	}
	
	public Filme() {
		 
	}

	public Filme(String titulo, String diretor, String categoria, String data, int id){
		this.Titulo = titulo;
		
		this.Diretor = diretor;
		
		this.Categoria = categoria;

		this.Lancamento = data;
		
		this.rowid = id;
	}

	public Filme(String titulo, String diretor, String categoria, String data){
		this.Titulo = titulo;
		
		this.Diretor = diretor;
		
		this.Categoria = categoria;

		this.Lancamento = data;
	}

	public Filme(String titulo, String diretor, String categoria, totalcross.util.Date data) {
	this.Titulo = titulo;
		
		this.Diretor = diretor;
		
		this.Categoria = categoria;

		this.Lancamento = data.getDate();
	}
 
 

}
